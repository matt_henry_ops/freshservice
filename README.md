# Freshservice plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```
composer require matt_henry_ops/freshservice
```

## Usage

This is a work in progress and is not recommended to use yet. It does work for some limitted endpoints. I will update this documentation once it is working fully.