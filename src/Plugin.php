<?php
declare(strict_types=1);

namespace Freshservice;

use Cake\Core\BasePlugin;
use Cake\Core\PluginApplicationInterface;
use Cake\Http\MiddlewareQueue;
use Cake\Routing\RouteBuilder;

/**
 * Plugin for Freshservice
 */
class Plugin extends BasePlugin
{

    /**
     * Disable routes hook.
     *
     * @var bool
     */
    protected $routesEnabled = false;

    /**
     * Disable middleware hook.
     *
     * @var bool
     */
    protected $middlewareEnabled = false;

    /**
     * Disable console hook.
     *
     * @var bool
     */
    protected $consoleEnabled = false;
    
    /**
     * Load all the plugin configuration and bootstrap logic.
     *
     * The host application is provided as an argument. This allows you to load
     * additional plugin dependencies, or attach events.
     *
     * @param \Cake\Core\PluginApplicationInterface $app The host application
     * @return void
     */
    public function bootstrap(PluginApplicationInterface $app): void
    {
        // Load Muffin Webservice for API calls
        $app->addPlugin('Muffin/Webservice');
    }

}
