<?php
namespace Freshservice\Model\Endpoint;

use Muffin\Webservice\Model\Endpoint;
use Cake\Utility\Inflector;
use Muffin\Webservice\Model\Schema;
use Cake\Event\Event;


class AssetsEndpoint extends FreshserviceEndpoint
{

    protected $_accessible = [
            '*' => true,
        ];

    public $initialSchema = [
        'id' => [
            'type' => 'integer',
        ],
        'display_id' => [
            'type' => 'integers',
        ],
        'name' => [
            'type' => 'string',
        ],
        'description' => [
            'type' => 'string',
        ],
        'asset_type_id' => [
            'type' => 'integer',
        ],
        'asset_tag' => [
            'type' => 'string',
        ],
        'impact' => [
            'type' => 'string',
        ],
        'author_type' => [
            'type' => 'string',
        ],
        'usage_type' => [
            'type' => 'string',
        ],
        'user_id' => [
            'type' => 'integer',
        ],
        'location_id' => [
            'type' => 'integer',
        ],
        'department_id' => [
            'type' => 'integer',
        ],
        'agent_id' => [
            'type' => 'integer',
        ],
        'group_id' => [
            'type' => 'integer',
        ],
        'assigned_on' => [
            'type' => 'datetime',
        ],
        'created_at' => [
            'type' => 'datetime',
        ],
        'updated_at' => [
            'type' => 'datetime',
        ],
        'type_fields' =>[
            'type' => 'json'
        ]
    ];

    /**
     * Initialize Method
     * 
     * @param array $config Array of config
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        // The primary key in which data is handled
        $this->setPrimaryKey('display_id');
        $this->setDisplayField('name');

        // Just like an entity this is the data the remotewebservice expects
        $schema = new Schema('Asset', $this->initialSchema);
        $this->setSchema($schema);
        
    }

    public function beforeMarshal(Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        
        // Convert empty strings to NULL
        /*foreach($data->getIterator() as $key => $value){
            if($data[$key] === ""){
                $data[$key] = null;
            }
        }*/
        // Pull non-schema elements and put in type_fields
        $data['type_fields'] = $this->buildTypeFields($data);
        //print_r($data);exit;
    }

    public function buildTypeFields(&$fields): array
    {
        $schema_columns = $this->getSchema()->columns();
        $type_fields = [];
        $fields_copy = $fields->getArrayCopy();
        foreach($fields_copy as $key => $value){
            if(in_array($key, $schema_columns)){
                continue;
            }else{
                $type_fields[$key] = $value;
                unset($fields[$key]);
            }
        }
        return array_filter($type_fields);
    }
    
}