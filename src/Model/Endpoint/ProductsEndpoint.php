<?php
namespace Freshservice\Model\Endpoint;

use Muffin\Webservice\Model\Endpoint;
use Cake\Utility\Inflector;
use Muffin\Webservice\Model\Schema;


class ProductsEndpoint extends FreshserviceEndpoint
{
    public function initialize(array $config): void
    {
        parent::initialize($config);
        
        // The primary key in which data is handled
        $this->setPrimaryKey('id');
        $this->setDisplayField('name');

        $schema = new Schema('Product', [
            'id' => [
                'type' => 'integer',
            ],
            'name' => [
                'type' => 'string',
            ],
            'asset_type_id' => [
                'type' => 'integer',
            ],
            'manufacturer' => [
                'type' => 'string',
            ],
            'status' => [
                'type' => 'string',
            ],
            'mode_of_procurement' => [
                'type' => 'string',
            ],
            'depreciation_type_id' => [
                'type' => 'integer',
            ],
            'description' => [
                'type' => 'string',
            ],
            'description_text' => [
                'type' => 'string',
            ],
            'created_at' => [
                'type' => 'datetime',
            ],
            'updated_at' => [
                'type' => 'datetime',
            ],

        ]);
        $this->setSchema($schema);
    }
}