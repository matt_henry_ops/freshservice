<?php
namespace Freshservice\Model\Endpoint;

use Muffin\Webservice\Model\Endpoint;
use Cake\Utility\Inflector;
use Muffin\Webservice\Model\Schema;


class AssetTypesEndpoint extends FreshserviceEndpoint
{
    public function initialize(array $config): void
    {
        parent::initialize($config);

        // The primary key in which data is handled
        $this->setPrimaryKey('id');
        $this->setDisplayField('name');

        $schema = new Schema('Asset', [
            'id' => [
                'type' => 'integer',
            ],
            'name' => [
                'type' => 'string',
            ],
            'parent_asset_type_id' => [
                'type' => 'integer',
            ],
            'description' => [
                'type' => 'string',
            ],
            'visible' => [
                'type' => 'boolean',
            ],
            'created_at' => [
                'type' => 'datetime',
            ],
            'updated_at' => [
                'type' => 'datetime',
            ],

        ]);
        $this->setSchema($schema);
    }
}