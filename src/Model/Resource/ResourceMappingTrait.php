<?php
namespace Freshservice\Model\Resource;

trait ResourceMappingTrait
{

    protected $status_map = [
        2 => 'Open',
        3 => 'Pending',
        4 => 'Resolved',
        5 => 'Closed',
    ];

    protected $source_map = [
        1 => 'Email',
        2 => 'Portal',
        3 => 'Phone',
        4 => 'Chat',
        5 => 'Feedback widget',
        6 => 'Yammer',
        7 => 'AWS Cloudwatch',
        8 => 'Pagerduty',
        9 => 'Walkup',
        10 => 'Slack',
    ];

    protected $priority_map = [
        1 => 'Low',
        2 => 'Medium',
        3 => 'High',
        4 => 'Urgent',
    ];

    protected $impact_map = [
        1 => 'low',
        2 => 'medium',
        3 => 'high',
    ];

    protected $usage_type_map = [
        1 => 'permanent',
        2 => 'loaner',
    ];

    protected $asset_state_map = [
		1 => 'In Use',
        2 => 'Missing',
        3 => 'In Transit',
        4 => 'In Stock',
        5 => 'Retired',
        6 => 'Retired (Recycled)',
        7 => 'Retired (Sold)',
        8 => 'Reserved',
    ];

    public function mapStatusToValue(int $id): string
    {
        return $this->status_map[$id];
    }
    public function mapSourceToValue(int $id): string
    {
        return $this->source_map[$id];
    }
    public function mapPriorityToValue(int $id): string
    {
        return $this->priority_map[$id];
    }
    public function mapImpactToValue(int $id): string
    {
        return $this->impact_map[$id];
    }
    public function mapUsageTypeToValue(int $id): string
    {
        return $this->usage_type_map[$id];
    }
    public function mapAssetStateToValue(int $id): string
    {
        return $this->asset_state_map[$id];
    }

    public function mapStatusToId(string $value): int
    {
        return array_flip($this->status_map)[$value];
    }
    public function mapSourceToId(string $value): int
    {
        return array_flip($this->source_map)[$value];
    }
    public function mapPriorityToId(string $value): int
    {
        return array_flip($this->priority_map)[$value];
    }
    public function mapImpactToId(string $value): int
    {
        return array_flip($this->impact_map)[$value];
    }
    public function mapUsageTypeToId(string $value): int
    {
        return array_flip($this->usage_type_map)[$value];
    }
    public function mapAssetStateToId(string $value): int
    {
        return array_flip($this->asset_state_map)[$value];
    }
}