<?php
namespace Freshservice\Model\Resource;

use Muffin\Webservice\Model\Resource;
use Cake\Controller\Controller;

class Asset extends Resource
{
    use ResourceMappingTrait;

    protected $_accessible = [
        '*' => true,
    ];

    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
    }

}