<?php
namespace Freshservice\Model\Resource;

use Muffin\Webservice\Model\Resource;

class Ticket extends Resource
{
    protected $status_map = [
        2 => 'Open',
        3 => 'Pending',
        4 => 'Resolved',
        5 => 'Closed',
    ];

    protected $source_map = [
        1 => 'Email',
        2 => 'Portal',
        3 => 'Phone',
        4 => 'Chat',
        5 => 'Feedback widget',
        6 => 'Yammer',
        7 => 'AWS Cloudwatch',
        8 => 'Pagerduty',
        9 => 'Walkup',
        10 => 'Slack',
    ];

    protected $priority_map = [
        1 => 'Low',
        2 => 'Medium',
        3 => 'High',
        4 => 'Urgent',
    ];

    public function getStatus()
    {
        return $this->status_map[$this->status];
    }

    public function getSourceType()
    {
        return $this->source_map[$this->source];
    }

    public function getPriority()
    {
        return $this->priority_map[$this->priority];
    }
}