<?php
namespace Freshservice\Model\Resource;

use Muffin\Webservice\Model\Resource;

class AssetTypeField extends Resource
{

    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
        foreach($this->fields as &$field){
            if(isset($field['choices'])){
                $field['options'] = $this->ConvertChoicesToOptions($field['choices']);
            }else{
                $field['options'] = [];
            }
        }
        
    }

    public function setOptionsByField($field_name, $options): void
    {
        $fieldIndex = array_search( $field_name, array_column($this->fields, 'name'));
        if($fieldIndex === false){
            // Throw Error?
        }else{
            $this->fields[$fieldIndex]['options'] = $options;
        }
    }

    /**
     * Return an array with Value => Value
     * You would think it would be ID => Value but the API for some reason doesn't want the ID when creating an asset
     * Used to convert the way the API returns choices
     */
    private function ConvertChoicesToOptions(array $choices): array
    {
        $return = array();
        foreach($choices as $choice){
            $return[$choice[0]] = $choice[0];
        }
        return $return;
    }

    

}

