<?php
namespace Freshservice\Webservice;

use Muffin\Webservice\Datasource\Query;
use Muffin\Webservice\Datasource\ResultSet;
use Muffin\Webservice\Webservice\Webservice;

class AssetsWebservice extends FreshserviceWebservice
{

    /**
     * {@inheritDoc}
     */
    public function initialize(): void
    {
        parent::initialize();
        $validOptions = [
            'include' =>
            [
                'type_fields',
            ],
        ];
        $this->setValidOptions($validOptions);

        $this->addNestedResource('/assets/:display_id', ['display_id']);
    }

    protected function _executeCreateQuery(Query $query, array $options = [])
    {
        // Get parameters and encode them for json.
        $queryParameters = $query->set();
        $data = json_encode($queryParameters, JSON_NUMERIC_CHECK);

        // Setup options
        $options = $this->getAuthentication();
        $options['type'] = 'json';
        $options['header']['Content-Type'] = 'application/json';
        
        // Perform post
        $response = $this->getDriver()->getClient()->post($this->getBaseUrl(), $data, $options);
        
        return $response->getJson();

        // Sudo code for converting response into asset object. Leaving in case it is needed later.
        #$this->_checkResponse($response);
        #$resources = $this->_transformResults($query->getEndpoint(), $results[$this->getEndpoint()] );
        #return new ResultSet($resources, count($resources));
    }

}