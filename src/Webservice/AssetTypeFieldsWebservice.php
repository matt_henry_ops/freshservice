<?php
namespace Freshservice\Webservice;

use Muffin\Webservice\Datasource\Query;
use Muffin\Webservice\Datasource\ResultSet;
use Muffin\Webservice\Webservice\Webservice;
use Cake\Utility\Inflector;
use Muffin\Webservice\Webservice\Exception\UnimplementedWebserviceMethodException;

class AssetTypeFieldsWebservice extends FreshserviceWebservice
{

    /**
     * {@inheritDoc}
     */
    public function initialize(): void
    {
        parent::initialize();
        $validOptions = [
        ];
        $this->setValidOptions($validOptions);
        $this->addNestedResource('/asset_types/:id/fields', ['id']);
        $this->addNestedResource('/asset_types/:AssetTypes.id/fields', ['AssetTypes.id']);
        #$this->setResultKey('asset_type_fields');
    }

    protected function _executeReadQuery(Query $query, array $options = [])
    {
        
        if ($query->clause('where')) {
            foreach ($query->clause('where') as $field => $value) {
                switch ($field) {
                    case 'AssetTypes.id':
                    case 'id':
                        #$this->setResultKey( Inflector::singularize( $this->getResultKey() ) );
                        break;
                    default:
                        throw new UnimplementedWebserviceMethodException( [
                            'name' => static::class,
                            'method' => 'where for field ' .  $field
                        ]);
                }
            }
        }
        return parent::_executeReadQuery($query, $options);
    }
}