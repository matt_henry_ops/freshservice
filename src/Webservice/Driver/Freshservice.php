<?php
namespace Freshservice\Webservice\Driver;

use Cake\Http\Client;
use Muffin\Webservice\Webservice\Driver\AbstractDriver;

use Cake\Datasource\ConnectionManager;

class Freshservice extends AbstractDriver
{
    /**
     * Initialize is used to easily extend the constructor.
     */
    public function initialize(): void
    {
        $this->setClient(new Client([
            'host' => $this->getConfig()['host'],
            'scheme' => $this->getConfig()['scheme'],
        ]));
    }

}