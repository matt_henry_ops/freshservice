<?php
namespace Freshservice\Webservice;

use Muffin\Webservice\Datasource\Query;
use Muffin\Webservice\Datasource\ResultSet;
use Muffin\Webservice\Webservice\Webservice;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

use Cake\Http\Session;
//use Cake\Core\Exception\Exception;
use App\Error\Exception\InvalidAPIOptionsException;
/**
 * Class Freshservice
 */

// Example code: https://github.com/CVO-Technologies/cakephp-github/blob/master/src/Webservice/GitHubWebservice.php

class FreshserviceWebservice extends Webservice
{

    protected $validOptions = [];
    protected $resultKey = '';

    /**
     * Initialize Webservice
     * 
     * @return void
     */
    public function initialize(): void
    {
        # Set the result key
        $this->resultKey = $this->getEndpoint();
        parent::initialize();
    }

    /**
     * Set resultKey used to access result of query
     * 
     * @return void
     */
    public function setResultKey(string $key): void
    {
        $this->resultKey = $key;
    }

    /**
     * Get resultKey
     * 
     * @return string Result Key
     */
    public function getResultKey(): string
    {
        return $this->resultKey;
    }

    /**
     * Returns the base URL for this endpoint
     *
     * @return string Base URL
     */
    public function getBaseUrl()
    {
        return '/' . $this->getEndpoint();
    }

    /**
     * Sets the valid options array
     * 
     * @return void
     */
    protected function setValidOptions(array $values): void
    {
        $this->validOptions = $values;
    }

    /**
     * Gets the valid options array
     * 
     * @return array
     */

    protected function getValidOptions(): array
    {
        return $this->validOptions;
    }

    /**
     * Gets authentication array
     * 
     * @return array
     */
    protected function getAuthentication(): array
    {
        $Users = TableRegistry::getTableLocator()->get('Users');
        $currentIdentity = Router::getRequest()->getAttribute('identity');
        $user = $Users->findByUsername($currentIdentity->username)->firstOrFail();
        return [ 'auth' => ['username' => ($user->profile->freshservice_api_key), 'password' => 'X']];
    }

    /**
     * Modifies queryParameters passed based on options supplied
     * 
     * @return void
     */

    protected function parseExtraOptions($options, &$queryParameters): void
    {
        $validOptions = $this->getValidOptions();
        foreach($options as $key => $value)
        {
            if(in_array($key, array_keys($validOptions)))
            {
                if(in_array($value, $validOptions[$key]))
                {
                    $queryParameters[$key] = $value;
                }else{
                    throw new InvalidAPIOptionsException( [ 'invalidOption' => $value, 'validOptions' => implode(', ', $validOptions[$key]) ] );
                }
            }else{
                throw new InvalidAPIOptionsException( [ 'invalidOption' => $key, 'validOptions' => implode(', ', array_keys($validOptions)) ] );
            }
        }
    }

    /**
     * Executes a query with the read action using the Cake HTTP Client
     */
    protected function _executeReadQuery(Query $query, array $options = [])
    {
        $url = $this->getBaseUrl();

        $queryParameters = [];
        // Page number has been set, add to query parameters
        if ($query->clause('page'))
        {
            $queryParameters['page'] = $query->clause('page');
        }

        // Result limit has been set, add to query parameters
        if ($query->clause('limit'))
        {
            $queryParameters['per_page'] = $query->clause('limit');
        }

        // Extra Options has been set, add to query parameters
        if($query->getOptions()){
            $this->parseExtraOptions($query->getOptions(), $queryParameters);
        }

        
        $search = false;
        $searchParameters = [];
        if ($query->clause('where')) {
            foreach ($query->clause('where') as $field => $value) {
                switch ($field) {
                    case 'id':
                        break;
                    default:
                        // Add the condition as search parameter
                        $searchParameters[$field] = $value;

                        // Mark this query as a search
                        $search = true;
                }
            }
        }
        
        
        // Check if this query could be requested using a nested resource.
        if ($nestedResource = $this->nestedResource($query->clause('where'))) {
            $url = $nestedResource;

            // If this is the case turn search of
            $search = false;
        }
        
        
        if ($search) {
            #$url = '/search' . $url;

            $q = [];
            foreach ($searchParameters as $parameter => $value) {

                $operation_symbol = ":";
                $first_character = substr($value, 0, 1);
                if( in_array($first_character, ['<','>']) ){
                    $operation_symbol .= $first_character;
                    $value = substr($value, 1) ?: "";
                }
                $q[] = $parameter . $operation_symbol . rawurlencode("'" . $value . "'");
            }

            $queryParameters['query'] = '"' . implode(rawurlencode(' AND '), $q) . '"';
        }

        $query_string = urldecode(http_build_query($queryParameters));

        /* @var Response $response */
        $response = $this->getDriver()->getClient()->get($url, $query_string, $this->getAuthentication());

        /* @var int $resourceAmount Total amount of resources */
        $resourceAmount = false;

        // Parse the Link header containing pagination info
        /*$links = $this->_parseLinks($response->header('Link'));
        if (isset($links['last'])) {
            $linkParameters = $this->_linkQueryParameters($links['last']);

            // Grab the last page number out of the Link header
            if ((isset($linkParameters['page'])) && (isset($linkParameters['per_page']))) {
                $resourceAmount = $linkParameters['page'] * $linkParameters['per_page'];
            }
        }*/

        $results = false;
        
        #if ($search) {
        #    $results = $response->getJson()[ $query->getEndpoint() ];

            #$resourceAmount = $response->json['total_count'];
        #}

        #if ($results === false) {
        #    $results = $response->getJson();
        #}
        $results = $response->getJson()[$this->getResultKey()];

        if(! is_array(current($results))){
            $results = array($results);
        }

        // Turn results into resources
        $resources = $this->_transformResults($query->getEndpoint(), $results );
        #print_r($resources);exit;
        #print_r(new ResultSet($resources, count($resources)));
        #exit;
         #debug(new ResultSet($resources, count($resources)));exit;
        return new ResultSet($resources, count($resources));
    }

    
    protected function _executeUpdateQuery(Query $query, array $options = [])
    {
        if ((!isset($query->where()['id'])) || (is_array($query->where()['id']))) {
            return false;
        }

        $url = $this->getBaseUrl();

        $parameters = $query->set();
        $parameters[$query->getEndpoint()->getPrimaryKey()] = $query->where()['id'];
        $queryParameters = array(
            Inflector::singularize($this->getEndpoint()) => $parameters
        );

        $response = $this->getDriver()->getClient()->put($url, $queryParameters, [ 'auth' => ['username' => ((new Session())->read('Freshservice.ApiKey')), 'password' => 'X']]);
    }

    protected function _executeCreateQuery(Query $query, array $options = [])
    {
        
    }
    /**
     * Modifies array passed by rawurlencoding each value
     */
    protected function urlencode_values($array): array
    {
        foreach($array as $key => $value){
            $array[$key] = rawurlencode($value);
        }

        return $array;
    }
}