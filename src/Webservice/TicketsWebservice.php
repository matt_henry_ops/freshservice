<?php
namespace Freshservice\Webservice;

use Muffin\Webservice\Datasource\Query;
use Muffin\Webservice\Datasource\ResultSet;
use Muffin\Webservice\Webservice\Webservice;

class TicketsWebservice extends FreshserviceWebservice
{

    /**
     * {@inheritDoc}
     */
    public function initialize(): void
    {
        parent::initialize();
    }
}