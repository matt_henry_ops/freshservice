<?php
namespace Freshservice\Webservice;

use Muffin\Webservice\Datasource\Query;
use Muffin\Webservice\Datasource\ResultSet;
use Muffin\Webservice\Webservice\Webservice;
use Cake\Utility\Inflector;
use Muffin\Webservice\Webservice\Exception\UnimplementedWebserviceMethodException;

class ProductsWebservice extends FreshserviceWebservice
{

    /**
     * {@inheritDoc}
     */
    public function initialize(): void
    {
        parent::initialize();
        $validOptions = [
            'include' => [],
        ];
        $this->setValidOptions($validOptions);
        $this->addNestedResource('/products/:id', ['id']);
        $this->addNestedResource('/products/:Products.id', ['AssetTypes.id']);
    }

    protected function _executeReadQuery(Query $query, array $options = [])
    {

        if ($query->clause('where')) {
            foreach ($query->clause('where') as $field => $value) {
                switch ($field) {
                    case 'AssetTypes.id':
                    case 'id':
                        $this->setResultKey( Inflector::singularize( $this->getResultKey() ) );
                        break;
                    default:
                        throw new UnimplementedWebserviceMethodException( [
                            'name' => static::class,
                            'method' => 'where for field ' .  $field
                        ]);
                }
            }
        }

        return parent::_executeReadQuery($query, $options);
    }
}